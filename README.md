## ROx
ROx (**R**ed **Ox**ide) is a 2D game development framework for Rust. It is in early development, and cannot do much yet.

## Dependencies
* [glutin](https://github.com/tomaka/glutin)
* [glium](https://github.com/tomaka/glium)
* [image](https://github.com/PistonDevelopers/image)