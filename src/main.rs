/*! A test for ROx. This file mimics what a developer using ROx would write.

*/

extern crate rox;
extern crate glium;

use rox::app::App;
use std::time::SystemTime;
use glium::glutin::{Event, ElementState, VirtualKeyCode};

fn main() {
	
	let mut cat_game = match App::mk(800, 600, "hello, cat".to_string(), false) {
		Some(game) => game, 
		None => panic!("Application couldn't be created"),
	};
		
	
	let big_tri = cat_game.polygon( vec!((-0.5, -0.5), (0.0, 0.5), (0.5, -0.25)), (0.4, 0.1, 0.0, 1.0) );
	let small_tri = cat_game.polygon( vec!((-0.9, 0.8), (-0.9, 0.9), (-0.8, 0.85)), (0.4, 0.1, 0.5, 1.0) );
	let quad = cat_game.quad( ( (0.9, 0.2), (0.8, -0.2) ), (0.8, 0.4, 0.6, 1.0) );
	let	dot = cat_game.point( (-0.8, -0.7), (1.0, 1.0, 1.0, 1.0) );
	let horizontal_line = cat_game.line( ( (0.0, -0.9), (0.7, -0.9) ), (0.6, 0.6, 0.3, 1.0) );
	let simple_poly = cat_game.polygon( vec!((-0.9, 0.9), (0.0, 0.0), (0.9, 0.9), (0.0, 0.99) ), (0.2, 0.3, 0.7, 1.0) );
	let weird_poly = cat_game.polygon( vec!((-0.9, 0.9), (0.0, 0.8), (0.0, 0.99), (0.9, 0.9) ), (0.5, 0.7, 0.7, 1.0) );
	
	// Game loop
	loop {
		let frame_time = SystemTime::now();
		
		// Get window events
		for event in cat_game.get_events() {
			match event {
				Event::Closed => return,
				Event::KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Escape)) => {
					return;
				},
				_ => ()
			}
		}		
		
		// Render. Blocked so the reference to cat_game's display field goes out of scope and is destroyed. Surely a better way to do this
		{
			let mut frame = cat_game.next_frame();
			frame.clear(0.2, 0.4, 0.3, 1.0);
			
			let draw_time = SystemTime::now();
			frame.draw_shape(&big_tri);
			frame.draw_shape(&small_tri);
			frame.draw_shape(&quad);
			frame.draw_shape(&dot);
			frame.draw_shape(&horizontal_line);
			frame.draw_shape(&simple_poly);
			frame.draw_shape(&weird_poly);
			print!("Draw time: {} us \t", (draw_time.elapsed().unwrap().subsec_nanos()/1000));
			
			let render_time = SystemTime::now();
			frame.render();
			print!("Render time: {} us \t", (render_time.elapsed().unwrap().subsec_nanos()/1000));
		}
		
		// Attempts to ensure constant FPS. Input is desired duration of each frame in milliseconds.
		cat_game.frame_duration(33);
		
		// Print frames per second
		println!("FPS: {}", (1000000000.0/(frame_time.elapsed().unwrap().subsec_nanos() as f32)));	
	}
}