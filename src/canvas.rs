use glium::{Frame, Surface, Program, uniforms};
use glium::backend::glutin_backend::GlutinFacade;
use graphics::Shape;

/// Canvas is a wrapper for Glium's Frame struct. A instance of canvas should be created and destructed for each frame.  
pub struct Canvas<'a, 'b, 'c> {
	frame: Frame,
	display: &'a GlutinFacade,
	shape_progam: &'b Program,
	image_progam: &'c Program
}
impl<'a, 'b, 'c> Canvas<'a, 'b, 'c> {
	/// Attempts to create new Canvas object.
	pub fn mk(frame: Frame, display: &'a GlutinFacade, shape_progam: &'b Program, image_progam: &'c Program) -> Option<Self> {
		Some( Canvas{ frame: frame, display: display, shape_progam: shape_progam, image_progam: image_progam } )
	}
	
	/// Creates a background color.
	pub fn clear(&mut self, red: f32, green: f32, blue: f32, alpha: f32) {
		self.frame.clear_color(red, green, blue, alpha);
	}
	
	/// Draws shape to frame buffer. Takes a reference to an instance of Shape.
	pub fn draw_shape<'d>(&mut self, shape: &'d Shape) -> () {		
		self.frame.draw(shape.get_vertex_buffer(),
						shape.get_index_buffer(),
						self.shape_progam,
						&uniforms::EmptyUniforms,
						&Default::default()).unwrap()
	}
	
	/// Destructs this instance of Canvas and displays it.
	pub fn render(self) {
		self.frame.finish().unwrap();
	}
}
