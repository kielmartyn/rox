use glium::vertex::VertexBuffer;
use glium::index::{IndexBuffer, PrimitiveType};
use glium::backend::glutin_backend::GlutinFacade;

/// Contains a reference to a vertex buffer representing a polygon as well as a number of functions to manipulate the data in the buffer. Will eventually be able to draw the shape from here as well.
pub struct Shape {
	vertex_buffer: VertexBuffer<ShapeVertex>,
	index_buffer: IndexBuffer<u8>
}
impl Shape {
	/// Create a simple shape object (no indices)
	pub fn mk( display: &GlutinFacade, coordinates: Vec<(f32, f32)>, color: (f32, f32, f32, f32) ) -> Shape {
		let coordinates_len = coordinates.len();		
		
		// Convert data into ImageVertex object
		let mut vertices_data: Vec<ShapeVertex> = Vec::new();
		for coordinate in coordinates {
			let vertex = ShapeVertex { position: [coordinate.0, coordinate.1], in_color: [color.0, color.1, color.2, color.3] };
			vertices_data.push(vertex);
		}

		// Create index vector
		let mut index_vector: Vec<u8> = Vec::new();
		for i in 0..(coordinates_len - 2) {
			index_vector.push(0 as u8);
			index_vector.push((i + 1) as u8);
			index_vector.push((i + 2) as u8);
		}
		for x in 0..index_vector.len() {
			print!("{}, ", index_vector[x]);
		}
		println!("");
		
		// Convert to buffers
		let vertex_buffer = VertexBuffer::new(display, &vertices_data).unwrap();
	 	let index_buffer = IndexBuffer::new(display, PrimitiveType::TrianglesList, &index_vector).unwrap(); 
		
		Shape{ vertex_buffer: vertex_buffer, index_buffer: index_buffer }
	}
	
	/// Get reference to vertex buffer.
	pub fn get_vertex_buffer(&self) -> &VertexBuffer<ShapeVertex>{
		&self.vertex_buffer
	}
	
	/// Get reference to index buffer.
	pub fn get_index_buffer(&self) -> &IndexBuffer<u8>{
		&self.index_buffer
	}
	
	// pub fn draw(&self) {
	// 	println!("draw");
	// }
	
	// pub fn move(&self) {
	// 	println!("move");
	// }
	
	// pub fn resize(&self) {
	// 	println!("resize");
	// }
	
	// pub fn rotate(&self) {
	// 	println!("rotate");
	// }
}

/// Contains a reference to a vertex buffer representing an image as well as  a number of functions to manipulate the data in the buffer. Will eventually be able to draw the shape from here as well.
pub struct Image {
	buffer: VertexBuffer<ImageVertex>
}
impl Image {
	/// Create new shape object.
	pub fn mk_simple( display: &GlutinFacade, coordinates: Vec<(f32, f32)> ) -> Image {
		// Vertices data container
		let mut vertices_data: Vec<ImageVertex> = Vec::new();
		
		// Convert data into ImageVertex
		for coordinate in coordinates {
			let vertex = ImageVertex { position: [coordinate.0, coordinate.1], tex_pos: [0.0, 0.0] };
			vertices_data.push(vertex);
		}
		
		// let vertex3 = ShapeVertex { position: [(coordinates.2).0, (coordinates.2).1], in_color: [color.0, color.1, color.2, color.3] };
		// let vertices_data = vec![vertex1, vertex2, vertex3];

		// Convert to a buffer
		let buffer = VertexBuffer::new(display, &vertices_data).unwrap();
		
		Image{ buffer: buffer }
	}
}

#[derive(Copy, Clone)]
pub struct ShapeVertex {
	position: [f32; 2],
	in_color: [f32; 4],
}
implement_vertex!(ShapeVertex, position, in_color);

#[derive(Copy, Clone)]
pub struct ImageVertex {
	position: [f32; 2],
	tex_pos: [f32; 2],
}
implement_vertex!(ImageVertex, position, tex_pos);
