use glium::glutin::{WindowBuilder};
use glium::{DisplayBuild, Surface, Program, glutin};
use glium::backend::glutin_backend::{GlutinFacade, PollEventsIter};
use std::{thread};
use std::time::{Duration, SystemTime};
use canvas::Canvas;
use graphics::{Shape, Image};

/// App is the main ROx object. More specifically it is wrapper for a GlutinFacade, a window and OpenGL context. Instances of App must be mutable.
pub struct App {
	display: GlutinFacade,
	width: u32,
	height: u32,
	frame_start_time: SystemTime,
	shape_program: Program,
	image_program: Program
}
impl App {
	/// Creates a new instance of App.
	pub fn mk(width: u32, height: u32, name: String, fullscreen: bool) -> Option<Self> {
		let display = match fullscreen {
			true => {
				match WindowBuilder::new()
					.with_fullscreen(glutin::get_primary_monitor())
					.with_title(name)
					.build_glium() {
						Ok(window) => window,
						Err(error) => {
							println!("{}", error);
							return None
						}
				}
			},
			
			false => {
				match WindowBuilder::new()
					.with_dimensions(width, height)
					.with_title(name)
					.build_glium() {
						Ok(window) => window,
						Err(error) => {
							println!("{}", error);
							return None
						}
				}
			}
		};
				
		let shape_program = App::generate_shape_program(&display);
		let image_program = App::generate_image_program(&display);
				
		Some( App{ display: display, width: width, height: height, frame_start_time: SystemTime::now(),
				   shape_program: shape_program, image_program: image_program } )
	}
	
	/// Returns the iterator of events that occured.
	pub fn get_events(&self) -> PollEventsIter {
		self.display.poll_events()
	}
	
	/// Create next frame. Used for rendering.
	pub fn next_frame(&self) -> Canvas {
		match Canvas::mk( self.display.draw(), self.get_display_ref(), self.get_shape_program(), self.get_image_program() ) {
			Some(canvas) => return canvas,
			None => panic!("canvas couldn't be created")
		}
	}
	
	/// Get reference to display
	pub fn get_display_ref(&self) -> &GlutinFacade {
		&self.display
	}
	
	/// Helps ensure that FPS is constant. Should be last call in main game loop. Input is milliseconds per frame. 
	pub fn frame_duration(&mut self, frame_time: u64) {
		// Get time at end of frame
		let current_time = SystemTime::now();
		// Get time since end of last frame, must times by 1000 to get in milliseconds
		let actual_duration = current_time.duration_since(self.frame_start_time).unwrap();
		// Create a Duration object from frame_time  
		let target_duration = Duration::from_millis(frame_time);
		
		// Only sleep if target is greater than actual. Also, they are unsigned ints, so a negative result is a runtime error.
		if target_duration > actual_duration {
			// Sleep the thread for time equal to the difference of target frame time and the actual frame time
			thread::sleep(target_duration - actual_duration);
		}
				
		// Set next ƒrame's start time
		self.frame_start_time = SystemTime::now();
	}
	
	/// Draw any kind of polygon. To avoid getting weird shapes, the order of vertex coordinates should be in order of clockwise or counterclockwise.
	pub fn polygon(&self, coordinates: Vec<(f32, f32)>, color: (f32, f32, f32, f32)) -> Shape {
		Shape::mk(&self.display, coordinates, color)
	}
	
	/// Create a quad. Input should be opposite corners of quad.
	pub fn quad(&self, coordinates: ( (f32, f32), (f32, f32) ), color: (f32, f32, f32, f32)) -> Shape {
		// Create additional vertices and return shape object
		let coordinates_vec: Vec<(f32, f32)> = vec!(
			coordinates.0,
			( (coordinates.0).0, (coordinates.1).1 ),
			coordinates.1,
			( (coordinates.1).0, (coordinates.0).1 )
		);
		Shape::mk(&self.display, coordinates_vec, color)
	}
	
	/// Draw a point. IN PROGRESS. This is going to depend on updates to coordinate system.
	pub fn point(&self, coordinate: (f32, f32), color: (f32, f32, f32, f32)) -> Shape {
		// Get the height and width of pixels in terms of 2x2 window size
		let (pixel_width, pixel_height) = self.get_pixel_size();
		
		// Create adjusted coordinates for quad
		let adjusted_coordinate = ( (coordinate.0 - pixel_width, coordinate.1 - pixel_height),
									(coordinate.0 + pixel_width, coordinate.1 + pixel_height) ); 
		self.quad(adjusted_coordinate, color)
	}
	
	/// Draw a line. IN PROGRESS. This is going to depend on updates to coordinate system. Also, this logic doesn't account for slanted lines yet.
	pub fn line(&self, coordinates: ((f32, f32), (f32, f32)), color: (f32, f32, f32, f32)) -> Shape {
		// Get the height and width of pixels in terms of 2x2 window size
		let (pixel_width, pixel_height) = self.get_pixel_size();
		
		// Create adjusted coordinates for quad
		let adjusted_coordinate = ( ( (coordinates.0).0, (coordinates.0).1  - pixel_height),
									( (coordinates.1).0, (coordinates.1).1  + pixel_height) );
		self.quad(adjusted_coordinate, color)
	}
	
	// pub fn circle(&self, center: (f32, f32), radius: f32, color: (f32, f32, f32, f32)) -> Shape {
		
	// }
	
	/// Get size of pixels OpenGL, 2x2 coordinates. This will tie into future functionality regarding pixel coordinates.
	fn get_pixel_size(&self) -> (f32, f32) {
		( (2.0 / self.width as f32), (2.0 / self.height as f32) )
	}
	
	/// Create the program for shape rendering.
	fn generate_shape_program(display_ref: &GlutinFacade) -> Program {
		let vertex_shader_src = r#"
			#version 140
			in vec2 position;
			in vec4 in_color;
			out vec4 v_color;
			void main() {
				gl_Position = vec4(position, 0.0, 1.0);
				v_color = in_color;
			}
		"#;
		
		let fragment_shader_src = r#"
			#version 140
			in vec4 v_color;
			out vec4 color;
			void main() {
				color = v_color;
			}
		"#;
		
		Program::from_source(display_ref, &vertex_shader_src, &fragment_shader_src, None).unwrap()			
	}
	
	/// Get a reference to the shape program.
	pub fn get_shape_program(&self) -> &Program {
		&self.shape_program
	}
	
	/// Create the program for image/texture rendering.
	fn generate_image_program(display_ref: &GlutinFacade) -> Program {
		let vertex_shader_src = r#"
			#version 140
			in vec2 position;
			in vec2 tex_coords;
			out vec2 v_tex_coords;
			void main() {
				v_tex_coords = tex_coords;
				gl_Position = vec4(position, 0.0, 1.0);
			}
		"#;
		
		let fragment_shader_src = r#"
			#version 140
			in vec2 v_tex_coords;
			out vec4 color;
			uniform sampler2D tex;
			void main() {
				color = texture(tex, v_tex_coords);
			}
		"#;
		
		Program::from_source(display_ref, &vertex_shader_src, &fragment_shader_src, None).unwrap()			
	}
	
	/// Get a reference to the image program.
	pub fn get_image_program(&self) -> &Program {
		&self.image_program
	}
}