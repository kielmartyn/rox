#ROx Reference & Design Doc
	
## Milestones by Example

#### Windows
```
// create an instance of app
// includes creation of window and OpenGL context
// will also include event system eventually
let cat_game = app::mk(300, 300, "hello_cat", true);

// start app

cat_game.run()
```

#### Images
```
// create a reference image 
let cat = image::load("cat.jpg");

// create an instance of reference cat image
let mut cat_sprite = sprite::mk(cat, 50, 50);
```

#### Input & Events
```
// Not sure how I want this work yet, so syntax is janky.
def event key::up -> {
	cat.y = cat.y + 10;
}
```

#### Sounds
```
// create sound
let meow = sound::load("meow.wav");

// Event with crap syntax to play sound.
def event key::space -> {
	meow.play();
}
```

#### Drawing Shapes
```
// draw point, args: coordinates
graphics.draw_point(5, 30)

// draw triangle, args: vector of coordinates of corners, coordinates of corner 1
graphics.draw_triangle( ( (0,0), (50, 25), (50, -25) ), (100, 150) )

// draw quad, really just a convenience function that calls draw_tri() twice
// args: vector of coordinates of corners, coordinates of corner 1
graphics.draw_quad( ( (0,0), (50, 25), (70, 0), (50, -25) ), (300, 200) )

// draw line, calls triangle, args: coordinates of start and end points
graphics.draw_line( (525, 50), (150, 0) )

// draw circle, needs more research, args: radius, coordinate of midpoint
graphics.draw_circle( 15, (175, 50))
```
* Also need to be able to specify color. Maybe also shader.
* Dev should be able to make a coordinate vector before hand and pass it as a paramater or just pass the coordinates manually.
* All of these functions will probably just wrap draw_triangle(), so I can minimize the amount of code I have to write for Glium.


#### Animation
```

```

#### Text
```

```

## Objects

#### Application Struct
**Data**

* width, height
* name
* type
* 

**Functionality**: 


#### Images
* Images will be held in a dictionary with variable names as the key, and a pointer to an **Image** object.
* **Image** implementation:
	* data: position (get & set), dimensions (get & set), original dimensions (get), 
	* functions: reset_size, 

#### Sounds
* Very similar structure to Images, ie dictionary holding with key-value pairs of name to a **Sound** object	
* **Sound** implementation:
	* data: 
	* functions: 